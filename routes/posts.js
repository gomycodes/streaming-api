var express = require('express');
var router = express.Router();
const postController = require('../controllers/postController');

router.get('/', postController.all);
router.post('/', postController.create);
module.exports = router;