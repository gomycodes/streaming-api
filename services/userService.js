const UserModel = require('../models/usersModel');
const jwt = require("jsonwebtoken");
const secretKey = require("../config/jwt");

exports.register = async (req, res)=>{
  
  let userModel = new UserModel();
  userModel.firstname = req.body.firstname;
  userModel.lastname = req.body.lastname;
  userModel.email = req.body.email;
  userModel.username = req.body.username;
  userModel.roles =req.body.roles;

  const token = jwt.sign(req.body.email, secretKey.jwtConfig);
  userModel.password = token;
  
  const user = await userModel.save();
  return user;
}

exports.all = async (req, res)=>{

  const user = await UserModel.find();

  return user;
}

exports.one = async (req, res)=>{

  const user = await UserModel.findById(req.params.id);

  return user;
}

exports.login = async (req, res)=>{

  const authHeader = req.headers['Authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.status(401).json({message: "Acces denied"});

  jwt.verify(token, secretKey.jwtConfig, async (err, data) => {
    console.log(err)

    if (err) return res.status(403).json({message: "User not found"});

    const user = await UserModel.findOne({email: data.email});

    res.status(200).json(user);
  })

}
