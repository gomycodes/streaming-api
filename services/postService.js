let PostModel = require('../models/postModel');
const sendMail = require('./mailService');

exports.create = async (req, res)=>{
    let postModel = new PostModel();
    if(req.files){
        const fichier = req.files.image
        fichier.mv('./public/uploads/' + fichier.name);
        postModel.image = fichier.name;

        const video = req.files.image
        video.mv('./public/uploads/' + video.name);
        postModel.video = video.name;
    }

    postModel.title = req.body.title;
    postModel.description = req.body.description;
    postModel.type = req.body.type;
    postModel.categories = req.body.categories;


    const data = await postModel.save();

    await sendMail.sendMail("chezwilsmail@gmail.com", "Nouvelle publication sur GoMyStremain", "Bonjour Wil. Comment vas tu?");

    return data;

}

exports.all = async ()=>{

    const data = await PostModel.find().limit(12);
    return data;
}