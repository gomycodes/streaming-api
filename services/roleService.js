const RoleModel = require('../models/roleModel');

exports.create = async (req, res)=>{
    
    const roleModel = new RoleModel();
    roleModel.name = req.body.name
    roleModel.description = req.body.description

    const data = await roleModel.save();

    return data;
}

exports.update = async (req, res)=>{

    const roleId = req.params.id;
    const data = await RoleModel.findByIdAndUpdate(roleId, req.body)
    return data;
}

exports.destroy = async (req, res)=>{
    const roleId = req.params.id;
    const data = await RoleModel.findByIdAndDelete(roleId);
    return data;
}