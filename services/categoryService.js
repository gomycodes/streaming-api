const CategoryModel = require('../models/categoryModel');

exports.create = async (req)=>{

    let categoryModel = new CategoryModel();
    if(req.files){
        const fichier = req.files.image
        fichier.mv('./public/uploads/' + fichier.name);
        categoryModel.image = fichier.name;
    }

    categoryModel.title = req.body.title;
    categoryModel.description = req.body.description;

    const data = await categoryModel.save();

    return data;
}

exports.all = async ()=>{

    const data = await CategoryModel.find().limit(12);
    return data;
}