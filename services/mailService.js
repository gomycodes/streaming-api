"use strict";
const nodemailer = require("nodemailer");

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "in-v3.mailjet.com",
    port: 2525,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "6968b6e3b0a9052ec4343015ce2e4924", // generated ethereal user
      pass: "0483e070f683adb1fca4604494133a33", // generated ethereal password
    },
  });

  exports.sendMail = async (to, subject, content) =>{
    // send mail with defined transport object
    await transporter.sendMail({
        from: `GoMyStreaming 👻 <achilleaikpe@adjemin.com>`, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: content, // plain text body
        html: `<b>${content}</b>`, // html body
    });
  }