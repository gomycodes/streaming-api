const postService = require('../services/postService');

exports.create = async (req, res)=>{

    const data = await postService.create(req);
    res.status(200).json(data);
}

exports.all = async (req, res)=>{

    const data = await postService.all();
    res.status(200).json(data);
}