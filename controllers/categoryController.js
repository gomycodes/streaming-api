const categoryService = require('../services/categoryService');

exports.create = async (req, res)=>{

    const data = await categoryService.create(req);
    res.status(200).json(data);
}

exports.all = async (req, res)=>{

    const data = await categoryService.all();
    res.status(200).json(data);
}