const userService = require('../services/userService');

exports.register = async (req, res)=>{

    const data = await userService.register(req);
    res.status(200).json(data);
}

exports.all = async (req, res)=>{

    const data = await userService.all();
    res.status(200).json(data);
}

exports.login = async (req, res)=>{

    const data = await userService.login(req);
    res.status(200).json(data);
}