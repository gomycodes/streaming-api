const mongoose = require('mongoose')
const { Schema } = mongoose;

const userSchema = new mongoose.Schema({
    firstname : String,
    lastname : String,
    username : String ,
    email : String ,
    password : String ,
    roles : [{type: Schema.Types.ObjectId, ref: "Role", required: false}],
})

const UserModel = mongoose.model('User',userSchema)

module.exports = UserModel;

