const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
    title : String , 
    image : {type: String, required: false} ,
    description : {type: String, required: false} 
})

const CategoryModel = mongoose.model('Category',categorySchema)
module.exports = CategoryModel;
