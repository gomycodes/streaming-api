const mongoose = require('mongoose');
const {Schema} = mongoose;

const postSchema = new Schema({
    title: String,
    image: {type: String, required: false},
    video: String,
    description : {type: String, required: false},
    publishedAt: {type: Date, default: Date.now},
    type: String,
    categories: {type: Schema.Types.ObjectId, ref: 'Category'}
})

const PostModel = mongoose.model('Post', postSchema);

module.exports = PostModel;