const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema({
    name : String ,
    description : String
})

const RoleModel = mongoose.model('Role',roleSchema)
module.exports = RoleModel;
